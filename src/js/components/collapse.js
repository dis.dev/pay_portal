export default () => {
    const collapseControl = document.querySelectorAll('[data-collapse-control]');

    collapseControl.forEach(el => {
        el.addEventListener('click', (e) => {
            const self = e.currentTarget.closest('[data-collapse]');
            const control = self.querySelector('[data-collapse-control]');
            const content = self.querySelector('[data-collapse-content]');

            self.classList.toggle('open');

            if (self.classList.contains('open')) {
                control.setAttribute('aria-expanded', true);
                content.setAttribute('aria-hidden', false);
                content.style.maxHeight = content.scrollHeight + 'px';
            } else {
                control.setAttribute('aria-expanded', false);
                content.setAttribute('aria-hidden', true);
                content.style.maxHeight = null;
            }
            return false;
        });
    });
};
