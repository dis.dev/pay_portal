"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import AOS from  'aos';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')


// Меню для мобильной версии
mobileNav();

// Animation
AOS.init({
    duration: 1000,
    delay: 300
});

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-form-send]')) {
        document.querySelector('body').classList.add('modal-open')
    }

    if (event.target.closest('[data-modal-close]')) {
        document.querySelector('body').classList.remove('modal-open')
    }
})
